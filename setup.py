from setuptools import setup

with open('requirements.txt','r') as f:
    REQS=f.readlines()

setup(
    name='clockify-cli',
    version='0.1',
    install_requires=REQS,
    packages=[
        'clockify_cli'
    ],

    entry_points='''
        [console_scripts]
        clockify=clockify_cli.cli:cli
    ''',
)


