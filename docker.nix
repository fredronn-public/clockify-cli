let
    mach-nix = import (builtins.fetchGit {
        url = "https://github.com/DavHau/mach-nix";
        ref = "refs/tags/3.5.0";
    }) {};
    package = (import ./derivation.nix);
    image = mach-nix.mkDockerImage {
        packagesExtra = [
            package
        ];
    };
in
    image.override (oldAttrs: {
        name = "clockify";
        config.Cmd = [ "${package}/bin/clockify" ];
    })
