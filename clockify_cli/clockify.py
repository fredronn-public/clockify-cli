from clockify import factories
from datetime import date, datetime, timedelta
from workalendar.europe import Sweden
from pytimeparse.timeparse import timeparse

import pandas as pd
import os
import yaml

try:
    from yaml import CLoader as Loader, CDumper as Dumper
except ImportError:
    from yaml import Loader, Dumper

with open(os.getenv("CLOCKIFY_CONFIG", default="clockify.yaml")) as f:
    config = yaml.load(f, Loader=Loader)

workspace_services = factories.WorkspaceFactory(api_key=config.get('token'))
user_services = factories.UserFactory(api_key=config.get('token'))
timeentry_services = factories.TimeEntryFactory(api_key=config.get('token'))
project_services = factories.ProjectFactory(api_key=config.get('token'))

time_format = '%Y-%m-%dT%H:%M:%SZ'

workday = timedelta(hours=8)
work_calendar = Sweden()

def get_workspaces():
    return pd.DataFrame(workspace_services.get_all_workspaces())

def get_time_entries(workspace, user):
    return pd.DataFrame(timeentry_services.get_all_time_entry_user(workspace, user))

def filter_time_entries(timeEntries, project_list):
    not_in_list = timeEntries.apply(lambda x: x['projectId'] not in project_list, axis=1)
    return timeEntries.drop(not_in_list[not_in_list].index)

def workday(date: date):
    return work_calendar.is_working_day(date)

def get_flex_entries(workspace: str, user: str):
    flex = timedelta(seconds=0)
    times = filter_time_entries(get_time_entries(workspace, user), config.get('project_list'))
    duration = pd.DataFrame(times['timeInterval'].tolist())
    duration.sort_values(["start"], inplace=True)
    duration.drop_duplicates(subset=['start'], keep='last', inplace=True, ignore_index=True)
    no_duration = duration.apply(lambda x: x['duration'] is None, axis=1)
    duration.drop(no_duration[no_duration].index, inplace=True)
    # Add date field for all entries
    duration["date"] = [datetime.strptime(x, time_format).date() for x in duration["start"]]

    # Convert duration to timedelta
    duration["duration"] = [timedelta(seconds=timeparse(x[2:])) for x in duration["duration"]]

    # Summarise durations
    duration = duration.groupby('date', as_index=False)[['duration']].sum()

    duration["hours"] = [timedelta(hours=8) if workday(x) else timedelta(hours=0) for x in duration["date"]]
    duration["diff"] = duration["duration"].sub(duration["hours"], axis=0)
    duration["flex"] = duration["diff"].cumsum()
    return duration
