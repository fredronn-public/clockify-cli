#!/usr/bin/env python3
from typing import Optional
from pprint import pprint
from datetime import timedelta, datetime, date
from tabulate import tabulate
from clockify_cli.clockify import config, get_flex_entries, get_time_entries, get_workspaces
from clockify_cli.hass import update_counter
import typer
import os
import json
import pandas as pd

cli = typer.Typer()

@cli.command()
def workspaces():
    print(get_workspaces())

@cli.command()
def projects(workspace: str = config.get('workspace'), user:str = config.get('user')):
    tes = get_time_entries(workspace, user)
    print(set(tes['projectId'].tolist()))

@cli.command()
def flex(workspace: str = config.get('workspace'), user: str = config.get('user'), sum: bool = False, seconds: bool = False):
    flex = get_flex_entries(workspace, user)
    if sum:
        sum = flex.iloc[-1]["flex"]
        if seconds: print(int(sum.total_seconds()))
        else: print(f'{sum}')
    else:
        with pd.option_context('display.max_rows', None):
            print(flex[["date", "hours", "duration", "flex"]])

@cli.command()
def update_hass(entity_id, workspace: str = config.get('workspace'), user: str = config.get('user')):
    flex = get_flex_entries(workspace, user)
    if not update_counter(entity_id=entity_id, value=flex.iloc[-1]["flex"].total_seconds()):
        print("Update FAILED")
    else: print("Update OK")
    
if __name__ == "__main__":
    app()
