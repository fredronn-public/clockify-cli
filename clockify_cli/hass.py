import os
from requests import get, post

server = os.getenv('HASS_SERVER')
token = os.getenv('HASS_TOKEN')

base_url = f'{server}/api'
headers = {
    "Authorization": f'Bearer {token}',
    "content-type": "application/json",
}


def update_counter(entity_id, value):
    """ Update counter with new value """
    url = f'{base_url}/services/counter/set_value'
    response = post(url, headers = headers, json = {'entity_id': entity_id, 'value': value})
    return response.status_code in [ 200, 201 ]
